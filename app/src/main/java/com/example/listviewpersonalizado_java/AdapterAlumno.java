package com.example.listviewpersonalizado_java;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class AdapterAlumno extends ArrayAdapter<AlumnoItem> {
    // Declaración de atributos
    int groupId;
    Activity Context;
    ArrayList<AlumnoItem> listaAlumnos;
    LayoutInflater inflater;

    // Constructor
    public AdapterAlumno(int groupId, Activity Context, int id, ArrayList<AlumnoItem> listaAlumnos) {
        super(Context, id, listaAlumnos);
        this.listaAlumnos = listaAlumnos;
        inflater = (LayoutInflater) Context.getSystemService(android.content.Context.LAYOUT_INFLATER_SERVICE);
        this.groupId = groupId;
    }

    public View getView(int posicion, View convertView, ViewGroup parent) {
        View itemView = inflater.inflate(groupId, parent, false);
        ImageView imagenAlumno = (ImageView) itemView.findViewById(R.id.imgAlumnos);
        imagenAlumno.setImageResource(listaAlumnos.get(posicion).getFotoAlumno());
        TextView txtMatricula = (TextView) itemView.findViewById(R.id.lblMatriculas);
        txtMatricula.setText(listaAlumnos.get(posicion).getTxtMatricula());
        TextView txtNombreAlumno = (TextView) itemView.findViewById(R.id.lblNombres);
        txtNombreAlumno.setText(listaAlumnos.get(posicion).getTxtNombre());
        return itemView;
    }

    public View getDropDownView(int posicion, View convertView, ViewGroup parent) {
        return getView(posicion, convertView, parent);
    }
}
